//
//  TGAgentObject.h
//  travellerguide
//
//  Created by FAN CHING HUA on 2016/6/1.
//  Copyright © 2016年 footprint. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TGAgentObject : NSObject <NSURLSessionDelegate, NSURLSessionDataDelegate, NSURLSessionStreamDelegate>

typedef void (^searchAgentCompletion) (BOOL success, TGAgentObject *response, NSError *error);

@property (strong, nonatomic) NSString *agent_id;
@property (strong, nonatomic) NSString *agent_name;
@property (strong, nonatomic) NSString *cover;
@property (strong, nonatomic) NSString *picture;

- (void)setAgentId:(NSString *)person_id completion:(searchAgentCompletion)callback;


@end

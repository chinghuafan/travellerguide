//
//  LoginViewController.m
//  travellerguide
//
//  Created by FAN CHING HUA on 2016/5/12.
//  Copyright © 2016年 footprint. All rights reserved.
//

#import <ChameleonFramework/Chameleon.h>
#import "LoginViewController.h"
#import "TGAGlobalVariables.h"

@interface LoginViewController ()

@property (strong, nonatomic) NSURLSession *urlsession;

@end

@implementation LoginViewController
@synthesize loginButton, urlsession;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    loginButton = [[FBSDKLoginButton alloc] init];
    loginButton.center = self.view.center;
    loginButton.delegate = self;
    [self.view addSubview:loginButton];
    loginButton.readPermissions = @[@"email", @"public_profile", @"pages_show_list"];
    
    self.view.backgroundColor = [UIColor flatSkyBlueColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error {
    NSLog(@"the login result - %@", result.description);
    NSString *accessToken = [FBSDKAccessToken currentAccessToken].tokenString;
    NSString *userID = [FBSDKAccessToken currentAccessToken].userID;
    
//    [[FTGGlobalVariable sharedInstance] setUserID:userID];
    
    NSOperationQueue *operationQueue = [[NSOperationQueue alloc] init];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    urlsession = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:operationQueue];
    NSString *urlString = [NSString stringWithFormat:@"https://ec2-54-69-30-93.us-west-2.compute.amazonaws.com/users/%@/%@", userID, accessToken];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPMethod:@"GET"];
    
    NSURLSessionDataTask *dataTask = [urlsession dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (data == nil) {
            return;
        }
        NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSError *jsonError;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
        [[TGAGlobalVariables sharedInstance] setUserToken:[json valueForKey:@"long-token"]];
        NSLog(@"the response is %@", result);
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }];
    [dataTask resume];

}

- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton {
    
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler {
#warning This allows insecure SSL. We need to remove this from release builds.
    NSURLProtectionSpace * protectionSpace = challenge.protectionSpace;
    if ([protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        SecTrustRef serverTrust = protectionSpace.serverTrust;
        completionHandler(NSURLSessionAuthChallengeUseCredential, [NSURLCredential credentialForTrust: serverTrust]);
    }
    else {
        completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, nil);
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

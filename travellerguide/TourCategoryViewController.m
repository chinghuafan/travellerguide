//
//  TourCategoryViewController.m
//  travellerguide
//
//  Created by FAN CHING HUA on 2016/5/30.
//  Copyright © 2016年 footprint. All rights reserved.
//

#import "TourCategoryViewController.h"

@interface TourCategoryViewController ()

@end

@implementation TourCategoryViewController

- (instancetype)init {
    self = [super init];
    self.view = [[[NSBundle mainBundle] loadNibNamed:@"toursCategoryCard" owner:self options:nil] lastObject];
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

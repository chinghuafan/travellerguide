//
//  TourCategoryViewController.h
//  travellerguide
//
//  Created by FAN CHING HUA on 2016/5/30.
//  Copyright © 2016年 footprint. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TourCategoryViewController : UIViewController

@property (strong, nonatomic) NSString *toursName;
@property (strong, nonatomic) NSString *providerCount;
@property (strong, nonatomic) UIImage *toursThumbnail;

@end

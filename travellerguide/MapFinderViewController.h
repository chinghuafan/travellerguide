//
//  MapFinderViewController.h
//  travellerguide
//
//  Created by FAN CHING HUA on 2016/5/13.
//  Copyright © 2016年 footprint. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleMaps;

@interface MapFinderViewController : UIViewController <NSURLSessionDelegate, NSURLSessionDataDelegate, NSURLSessionStreamDelegate, UISearchBarDelegate, UINavigationControllerDelegate, GMSMapViewDelegate>

@end

//
//  main.m
//  travellerguide
//
//  Created by FAN CHING HUA on 2016/5/11.
//  Copyright © 2016年 footprint. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

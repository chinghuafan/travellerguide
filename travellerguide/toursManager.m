//
//  toursManager.m
//  travellerguide
//
//  Created by FAN CHING HUA on 2016/5/30.
//  Copyright © 2016年 footprint. All rights reserved.
//

#import "toursManager.h"
#import "TGAGlobalVariables.h"
#import "NSString+StringEncode.h"
#import "TGAgentObject.h"

@interface toursManager()

@property (strong, nonatomic) NSURLSession *urlsession;

@end

NSString *place = @"san francisco";
NSString *country = @"united states";
NSArray *activity;


@implementation toursManager

@synthesize agents_array, urlsession;

void (^searchCompletionCallback) (BOOL success, NSDictionary *response, NSError *error);

+ (instancetype) sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (instancetype) init {
    self = [super init];
    agents_array = [[NSMutableArray alloc] init];
    activity = [NSArray arrayWithObjects:@"bike", @"boat", @"diving", @"fishing", @"hiking", @"tour", nil];

//    [self executeGuidesSearch];
    
    return self;
}

//- (void)executeGuidesSearch:(NSString *)event completion:(void (^)(BOOL))completion {
- (void)executeGuidesSearch:(NSString *)region completion:(searchCompletion)callback {
    searchCompletionCallback = callback;

//- (void)executeGuidesSearch {
    [agents_array removeAllObjects];
    
    NSString *accessToken = [TGAGlobalVariables sharedInstance].userToken;
    place = [region stringByAddingPercentEncodingForRFC3986];
    country = [country stringByAddingPercentEncodingForRFC3986];
    NSOperationQueue *operationQueue = [[NSOperationQueue alloc] init];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    urlsession = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:operationQueue];
    
    for (int k = 0; k < activity.count; k++) {
        NSString *urlString = [NSString stringWithFormat:@"https://ec2-54-69-30-93.us-west-2.compute.amazonaws.com/agents/%@/%@/%@/%@", activity[k], place, country, accessToken];
        NSURL *url = [NSURL URLWithString:urlString];
        NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [urlRequest setHTTPMethod:@"GET"];
        
        NSURLSessionDataTask *dataTask = [urlsession dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            
            if (data == nil) {
                NSLog(@"the data is nil, url = %@", urlString);
                return;
            }
            NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSError *jsonError;
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
            NSLog(@"the response is %@", result);
            NSData *objectData = [[json objectForKey:@"return_value"] dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableContainers error:&jsonError];
            //load agents from server
            //insert the agents into manager
            NSArray *jsonData = [jsonDict objectForKey:@"data"];
            if (jsonData.count > 0) {
                NSMutableArray *agent_array = nil;
                NSMutableArray *local_agent = [[NSMutableArray alloc] init];
                NSString *agent_activity = [json objectForKey:@"activity"];
                agent_array = [jsonDict objectForKey:@"data"];
                if (agent_array.count > 0) {
                    for (int k = 0; k < agent_array.count; k++) {
                        TGAgentObject *agent = [[TGAgentObject alloc] init];
                        [agent setAgent_name:[[agent_array objectAtIndex:k] valueForKey:@"name"]];
                        [agent setAgentId:[[agent_array objectAtIndex:k] valueForKey:@"id"] completion:^(BOOL success, TGAgentObject *response, NSError *error) {
                            NSLog(@"the agent has completed");
                        }];
//                        [agent setAgentId:[[agent_array objectAtIndex:k] valueForKey:@"id"]] completion:^(BOOL success, TGAgentObject *response, NSError *error) {
//                            NSLog(@"the agent has completed");
//                        }];
//                        [agent setAgentId:[[agent_array objectAtIndex:k] valueForKey:@"id"]];
                        [local_agent addObject:agent];
                    }
                    NSMutableDictionary *agents_list = [[NSMutableDictionary alloc] init];
                    [agents_list setObject:agent_activity forKey:@"activity"];
                    [agents_list setObject:local_agent forKey:@"list"];
                    [agents_array addObject:agents_list];
                    NSLog(@"activity %@, total agent count %lu", [agents_list valueForKey:@"activity"], (unsigned long)local_agent.count);
                    searchCompletionCallback(YES, agents_list, nil);
                }
            }
            
            
        }];
        [dataTask resume];
    }
    
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler {
#warning This allows insecure SSL. We need to remove this from release builds.
    NSURLProtectionSpace * protectionSpace = challenge.protectionSpace;
    if ([protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        SecTrustRef serverTrust = protectionSpace.serverTrust;
        completionHandler(NSURLSessionAuthChallengeUseCredential, [NSURLCredential credentialForTrust: serverTrust]);
    }
    else {
        completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, nil);
    }
}


@end

//
//  MapFinderViewController.m
//  travellerguide
//
//  Created by FAN CHING HUA on 2016/5/13.
//  Copyright © 2016年 footprint. All rights reserved.
//

#import "MapFinderViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "TGAGlobalVariables.h"
#import "PlaceSearchResultViewController.h"
#import "GuideNameCardViewController.h"
#import "toursManager.h"

@interface MapFinderViewController ()

@property (strong, nonatomic) NSURLSession *urlsession;
@property (strong, nonatomic) UISearchController *searchController;
@property (strong, nonatomic) GuideNameCardViewController *guideProfile;
@property (strong, nonatomic) PlaceSearchResultViewController *pageView;
@property (strong, nonatomic) toursManager *manager;
//@property (strong, nonatomic) GMSMapView *mapCameraView;

@end

@implementation MapFinderViewController

@synthesize searchController, urlsession, pageView;

- (void)viewDidAppear:(BOOL)animated {
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:37.774212
                                                            longitude:-122.422053
                                                                 zoom:6];
    GMSMapView *mapCameraView = [GMSMapView mapWithFrame:self.view.frame camera:camera];
    mapCameraView.delegate = self;
    
    // Available map types: kGMSTypeNormal, kGMSTypeSatellite, kGMSTypeHybrid,
    // kGMSTypeTerrain, kGMSTypeNone
    
    mapCameraView.mapType = kGMSTypeTerrain;
    
    [self.view addSubview:mapCameraView];
    [mapCameraView animateToCameraPosition:camera];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.translucent = NO;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    searchController.searchResultsUpdater = nil;
    searchController.dimsBackgroundDuringPresentation = NO;
    searchController.searchBar.delegate = self;
    
    [self.view addSubview:self.searchController.searchBar];
    self.definesPresentationContext = YES;
    [searchController.searchBar sizeToFit];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
- (void) handleSingleTap : (UITapGestureRecognizer *)recognizer {
    
}
*/
- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    
    NSOperationQueue *operationQueue = [[NSOperationQueue alloc] init];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    urlsession = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:operationQueue];
    NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&key=%@", coordinate.latitude, coordinate.longitude, Google_API_Key];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPMethod:@"GET"];
    
    NSURLSessionDataTask *dataTask = [urlsession dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSError *jsonError;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
        NSArray *address_element = [json objectForKey:@"results"];
        
        for (int i = 0; i < address_element.count; i++) {
            NSDictionary *address_json = [address_element objectAtIndex:i];
            NSLog(@"try to find the region");
            NSArray *address_component = [address_json valueForKey:@"address_components"];
            if (address_component.count <= 2) {
                //to get boundary
                for (int k = 0; k < address_component.count; k++) {
                    NSDictionary *component_json = [address_component objectAtIndex:k];
                    NSLog(@"query region");
                    NSArray *address_list = [component_json valueForKey:@"types"];
                    
                    if ([[address_list objectAtIndex:0] containsString:@"administrative_area_level_1"]) {
                        //start to query the agents by the region
                        NSString *region_name = [component_json valueForKey:@"long_name"];
                        _manager = [toursManager sharedInstance];
                        [_manager executeGuidesSearch:region_name completion:^(BOOL success, NSDictionary *response, NSError *error) {
                            if (success) {
                                NSLog(@"got the response");
                            }
                        }];
                        
                        //the administrative 1 has founded
                        NSDictionary *geometry_list = [address_json valueForKey:@"geometry"];
                        NSDictionary *bounds = [geometry_list valueForKey:@"bounds"];
                                NSLog(@"the bounds has founded");
                        NSDictionary *northeast = [bounds valueForKey:@"northeast"];
                        NSDictionary *southwest = [bounds valueForKey:@"southwest"];
                        NSString *latstring = [NSString stringWithFormat:@"%.6f", [[northeast valueForKey:@"lat"] doubleValue]];
                        NSString *lonstring = [NSString stringWithFormat:@"%.6f", [[northeast valueForKey:@"lng"] doubleValue]];
                        
                        NSDecimalNumber *number = [NSDecimalNumber decimalNumberWithString:latstring];
                        double latdouble = [number doubleValue];
//                        double latdouble = [latstring doubleValue];
                        double londouble = [lonstring doubleValue];
                        CLLocationCoordinate2D northCoord2D = CLLocationCoordinate2DMake(latdouble, londouble);
                        
                        latstring = [southwest valueForKey:@"lat"];
                        lonstring = [southwest valueForKey:@"lng"];
                        latdouble = [latstring doubleValue];
                        londouble = [lonstring doubleValue];
                        CLLocationCoordinate2D southCoord2D = CLLocationCoordinate2DMake(latdouble, londouble);
                        GMSCoordinateBounds *bounds_region = [[GMSCoordinateBounds alloc] initWithCoordinate:northCoord2D coordinate:southCoord2D];
                        GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds_region];
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            GMSMarker *mark1 = [GMSMarker markerWithPosition:northCoord2D];
                            GMSMarker *mark2 = [GMSMarker markerWithPosition:southCoord2D];
                            [mark1 setMap:mapView];
                            [mark2 setMap:mapView];
                            [mapView animateWithCameraUpdate:update];
                            
                            
                            /*
                            if (pageView == nil) {
                                pageView = [[PlaceSearchResultViewController alloc] init];
                                [pageView.view setFrame:CGRectMake(0, 0, 320, 240)];
                                [pageView.view setBackgroundColor:[UIColor colorWithRed:(30.0f/255) green:(30.0f/255) blue:(30.0f/255) alpha:0.6]];
                                
                                [pageView.view setTranslatesAutoresizingMaskIntoConstraints:NO];
                                [self.view addSubview:pageView.view];
                            } else {
                                
                            }

                            NSDictionary *viewsPagesDictionary = @{@"pageView" : pageView.view};
                            
                            [self.view addConstraints:[NSLayoutConstraint
                                                      constraintsWithVisualFormat:@"|-20-[pageView]-20-|"
                                                      options:NSLayoutFormatAlignAllBaseline metrics:nil
                                                       views:viewsPagesDictionary]];
                            [self.view addConstraints:[NSLayoutConstraint
                                                       constraintsWithVisualFormat:@"V:|-120-[pageView(240)]"
                                                       options:NSLayoutFormatAlignAllBaseline metrics:nil
                                                       views:viewsPagesDictionary]];
                            
 
                            UILabel *lblPlace = [[UILabel alloc] initWithFrame:CGRectMake(0, pageView.view.bounds.origin.y + 40, pageView.view.frame.size.width, 70.0f)];
                            [lblPlace setText:[NSString stringWithFormat:@"explorer in %@", [component_json valueForKey:@"long_name"]]];
                            [self.view addSubview:lblPlace];
                            [lblPlace setTranslatesAutoresizingMaskIntoConstraints:NO];
                            NSDictionary *viewsDictionary = @{@"lblPlace" : lblPlace};
                            NSArray *constraintsArray = [NSLayoutConstraint
                                                         constraintsWithVisualFormat:@"|-120-[lblPlace(240)]"
                                                         options:NSLayoutFormatAlignAllBaseline metrics:nil
                                                         views:viewsDictionary];
                            [self.view addConstraints:constraintsArray];
                            
                            
                            _guideProfile = [[GuideNameCardViewController alloc] init];
                            [_guideProfile.view setTranslatesAutoresizingMaskIntoConstraints:NO];
                            [pageView.view addSubview:_guideProfile.view];
                            NSDictionary *viewsProfilesDictionary = @{@"_guideProfile" : _guideProfile.view};
                            
                            [pageView.view addConstraints:[NSLayoutConstraint
                                                       constraintsWithVisualFormat:@"|-20-[_guideProfile]-20-|"
                                                       options:NSLayoutFormatAlignAllBaseline metrics:nil
                                                       views:viewsProfilesDictionary]];
                            
                            [pageView.view addConstraint:[NSLayoutConstraint constraintWithItem:pageView.view attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_guideProfile.view attribute:NSLayoutAttributeTop multiplier:1.0 constant:-25]];
                             */
                            
                       });
                        
                        
                    }
                }
            }
        }
        
        NSLog(@"the result has return");
    }];
    [dataTask resume];

}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler {
#warning This allows insecure SSL. We need to remove this from release builds.
    NSURLProtectionSpace * protectionSpace = challenge.protectionSpace;
    if ([protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        SecTrustRef serverTrust = protectionSpace.serverTrust;
        completionHandler(NSURLSessionAuthChallengeUseCredential, [NSURLCredential credentialForTrust: serverTrust]);
    }
    else {
        completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, nil);
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

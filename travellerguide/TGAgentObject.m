//
//  TGAgentObject.m
//  travellerguide
//
//  Created by FAN CHING HUA on 2016/6/1.
//  Copyright © 2016年 footprint. All rights reserved.
//

#import "TGAGlobalVariables.h"
#import "TGAgentObject.h"

@interface TGAgentObject()

@property (strong, nonatomic) NSURLSession *urlsession;

@end


@implementation TGAgentObject

@synthesize agent_id, agent_name, urlsession;

void (^searchAgentCompletionCallback) (BOOL success, TGAgentObject *response, NSError *error);

- (void)setAgentId:(NSString *)person_id completion:(searchAgentCompletion)callback {
//- (void)setAgentId:(NSString *)person_id {
    if (person_id == nil) {
        return;
    }
    searchAgentCompletionCallback = callback;
    self.agent_id = person_id;
    NSString *urlString = [NSString stringWithFormat:@"https://ec2-54-69-30-93.us-west-2.compute.amazonaws.com/agent/info/%@/%@", agent_id, [TGAGlobalVariables sharedInstance].userToken];
    
    NSOperationQueue *operationQueue = [[NSOperationQueue alloc] init];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    urlsession = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:operationQueue];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPMethod:@"GET"];
    
    NSURLSessionDataTask *dataTask = [urlsession dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (data == nil) {
            return ;
        }
        //        NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSError *jsonError;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
        //        NSLog(@"the response is %@", result);
        NSData *objectData = [[json objectForKey:@"return_value"] dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableContainers error:&jsonError];
        NSDictionary *jsonPicture = [jsonDict valueForKey:@"picture"];
        NSDictionary *picture_data = [jsonPicture valueForKey:@"list"];
        NSDictionary *jsonCover = [jsonDict valueForKey:@"cover"];
        searchAgentCompletionCallback(YES, self, nil);
        
        /*
        cover = [jsonCover valueForKey:@"source"];
        about = [jsonDict valueForKey:@"about"];
        [jsonDict valueForKey:@"single_line_address"];
        NSDictionary *myLocationDict = [jsonDict valueForKey:@"location"];
        self.city = [myLocationDict valueForKey:@"city"];
        self.country = [myLocationDict valueForKey:@"country"];
        address = [jsonDict valueForKey:@"single_line_address"];
        NSURL *imageURL = [NSURL URLWithString:cover];
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        title_cover = [UIImage imageWithData:imageData];
        NSLog(@"the title cover has setting");
        
        picture = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=150&height=150", self.agent_id];
        NSURL *pictureURL = [NSURL URLWithString:picture];
        NSData *pictureData = [NSData dataWithContentsOfURL:pictureURL];
        profile_picture = [UIImage imageWithData:pictureData];
        self.bLoading = NO;
        [self.delegate AgentInfoCompleted:self];
         */
        
    }];
    [dataTask resume];
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler {
#warning This allows insecure SSL. We need to remove this from release builds.
    NSURLProtectionSpace * protectionSpace = challenge.protectionSpace;
    if ([protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        SecTrustRef serverTrust = protectionSpace.serverTrust;
        completionHandler(NSURLSessionAuthChallengeUseCredential, [NSURLCredential credentialForTrust: serverTrust]);
    }
    else {
        completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, nil);
    }
}

@end

//
//  TGAGlobalVariables.h
//  travellerguide
//
//  Created by FAN CHING HUA on 2016/5/13.
//  Copyright © 2016年 footprint. All rights reserved.
//

#define Google_API_Key      @"AIzaSyBB4NyHMNRY7vvouvuc-MnLkTi-3axUubE"
#define Google_Service_API_Key  @"AIzaSyCr91x3IYrWU4IRvHuRg5SzoR4Ipj8Tcz4"

#import <Foundation/Foundation.h>
@import CoreLocation;

@interface TGAGlobalVariables : NSObject

@property (strong, nonatomic) NSString *userID;
@property (strong, nonatomic) NSString *userToken;
@property (strong, nonatomic) NSString *country;

+ (instancetype) sharedInstance;

@end

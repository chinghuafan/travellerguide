//
//  TGAGlobalVariables.m
//  travellerguide
//
//  Created by FAN CHING HUA on 2016/5/13.
//  Copyright © 2016年 footprint. All rights reserved.
//


#import "TGAGlobalVariables.h"

@implementation TGAGlobalVariables

+ (instancetype) sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (instancetype) init {
    self = [super init];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObject:@"en"] forKey:@"AppleLanguages"];
    
    return self;
}

@end

//
//  toursManager.h
//  travellerguide
//
//  Created by FAN CHING HUA on 2016/5/30.
//  Copyright © 2016年 footprint. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface toursManager : NSObject <NSURLSessionDelegate, NSURLSessionDataDelegate, NSURLSessionStreamDelegate>

typedef void (^searchCompletion) (BOOL success, NSDictionary *response, NSError *error);

@property (strong, nonatomic) NSMutableArray *agents_array;

+ (instancetype) sharedInstance;
- (void)executeGuidesSearch: (NSString *)region completion:(searchCompletion)callback;

@end

//
//  AppDelegate.h
//  travellerguide
//
//  Created by FAN CHING HUA on 2016/5/11.
//  Copyright © 2016年 footprint. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) BOOL authenticated;


@end


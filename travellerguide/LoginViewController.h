//
//  LoginViewController.h
//  travellerguide
//
//  Created by FAN CHING HUA on 2016/5/12.
//  Copyright © 2016年 footprint. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface LoginViewController : UIViewController <FBSDKLoginButtonDelegate, NSURLSessionDelegate, NSURLSessionDataDelegate, NSURLSessionStreamDelegate>

@property (strong, nonatomic) IBOutlet FBSDKLoginButton *loginButton;

@end

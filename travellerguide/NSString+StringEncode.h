//
//  NSString+StringEncode.h
//  travellerguide
//
//  Created by FAN CHING HUA on 2016/5/31.
//  Copyright © 2016年 footprint. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (StringEncode)

- (nullable NSString *)stringByAddingPercentEncodingForRFC3986;

@end

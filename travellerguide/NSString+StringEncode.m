//
//  NSString+StringEncode.m
//  travellerguide
//
//  Created by FAN CHING HUA on 2016/5/31.
//  Copyright © 2016年 footprint. All rights reserved.
//

#import "NSString+StringEncode.h"

@implementation NSString (StringEncode)

- (nullable NSString *)stringByAddingPercentEncodingForRFC3986 {
    NSString *unreserved = @"-._~/?";
    NSMutableCharacterSet *allowed = [NSMutableCharacterSet
                                      alphanumericCharacterSet];
    [allowed addCharactersInString:unreserved];
    return [self
            stringByAddingPercentEncodingWithAllowedCharacters:
            allowed];
}


@end
